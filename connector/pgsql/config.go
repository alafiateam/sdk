package pgsql

import "time"

type Config struct {
	HostName          string
	UserName          string
	Password          string
	Port              int
	DataBaseName      string
	MaxOpenConnection int
	MaxIdleConnection int
	MaxLifetime       time.Duration
}
