package pgsql

import (
	"fmt"
	"os"
	"strconv"
	"time"

	"github.com/jmoiron/sqlx"
	_ "github.com/lib/pq"
)

type DB struct {
	*sqlx.DB
}

func Open() (*DB, error) {
	var (
		hostName     = os.Getenv("POSTGRES_HOSTNAME")
		userName     = os.Getenv("POSTGRES_USERNAME")
		password     = os.Getenv("POSTGRES_PASSWORD")
		port         = os.Getenv("POSTGRES_PORT")
		dataBaseName = os.Getenv("POSTGRES_DB")
	)
	p, err := strconv.Atoi(port)
	if err != nil {
		return nil, err
	}
	maxIdleConnection := 0
	if m, err := strconv.Atoi(os.Getenv("POSTGRES_MAX_IDLE_CONNECTION")); err != nil {
		maxIdleConnection = m
	}

	maxOpenConnection := 0
	if m, err := strconv.Atoi(os.Getenv("POSTGRES_MAX_OPEN_CONNECTION")); err != nil {
		maxOpenConnection = m
	}

	maxLifetime := 0
	if m, err := strconv.Atoi(os.Getenv("POSTGRES_MAX_LIFE_TIME")); err != nil {
		maxLifetime = m
	}

	return OpenWithConfig(Config{
		HostName:          hostName,
		UserName:          userName,
		Password:          password,
		Port:              p,
		DataBaseName:      dataBaseName,
		MaxIdleConnection: maxIdleConnection,
		MaxOpenConnection: maxOpenConnection,
		MaxLifetime:       time.Duration(maxLifetime),
	})
}

func OpenWithConfig(config Config) (*DB, error) {
	connectionString := fmt.Sprintf("host=%s port=%d user=%s password=%s dbname=%s sslmode=disable",
		config.HostName, config.Port, config.UserName, config.Password, config.DataBaseName)
	connection := sqlx.MustOpen("postgres", connectionString)
	err := connection.Ping()
	if err != nil {
		panic(err)
	}
	connection.SetMaxIdleConns(config.MaxIdleConnection)
	connection.SetMaxOpenConns(config.MaxOpenConnection)
	connection.SetConnMaxLifetime(config.MaxLifetime * time.Second)

	return &DB{connection}, nil
}
