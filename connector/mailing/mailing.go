package mailing

import (
	"os"
	"strconv"

	"gitlab.com/alafiateam/sdk/errors"
	"gopkg.in/gomail.v2"
)

type Server struct {
	Host     string
	User     string
	Password string
	Port     int
}

func (s *Server) isValid() bool {
	return s.Host != "" && s.User != "" && s.Password != "" && s.Port > 0
}

type Mailing interface {
	Send(sender, recipient, subject, message string) error
}

type mailing struct {
	*gomail.Dialer
}

func New() (Mailing, error) {
	p, err := strconv.Atoi(os.Getenv("SMTP_PORT"))
	if err != nil {
		return nil, err
	}
	s := Server{
		Host:     os.Getenv("SMTP_SERVER"),
		User:     os.Getenv("SMTP_USER"),
		Password: os.Getenv("SMTP_PASSWORD"),
		Port:     p,
	}
	return NewWithConfig(s)
}

func NewWithConfig(s Server) (Mailing, error) {
	if !s.isValid() {
		return nil, errors.MissingMandatory()
	}
	d := gomail.NewDialer(s.Host, s.Port, s.User, s.Password)
	return &mailing{d}, nil
}

func (m mailing) Send(sender, recipient, subject, body string) error {
	if len(sender) == 0 || len(recipient) == 0 || len(subject) == 0 || len(body) == 0 {
		return errors.MissingMandatory()
	}
	message := gomail.NewMessage()
	message.SetHeader("From", sender)
	message.SetHeader("To", recipient)
	message.SetHeader("Subject", subject)
	message.SetBody("text/html", body)

	return m.DialAndSend(message)
}
