package aws

import (
	"fmt"
	"mime/multipart"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/aws/aws-sdk-go/aws"
	"github.com/aws/aws-sdk-go/aws/session"
	"github.com/aws/aws-sdk-go/service/s3"
	"github.com/aws/aws-sdk-go/service/s3/s3manager"
	"github.com/aws/aws-sdk-go/service/ses"
	"gitlab.com/alafiateam/sdk/errors"
)

const CHARSET = "UTF-8"

type Config struct {
	s3Uploader *s3manager.Uploader
	s3Client   *s3.S3
	sesClient  *ses.SES
}

type MailInput struct {
	From     string
	To       []string
	Subject  string
	HtmlText string
}

func NewAws(sess *session.Session) Config {
	return Config{
		s3Uploader: s3manager.NewUploader(sess),
		s3Client:   s3.New(sess),
		sesClient:  ses.New(sess),
	}
}

var types = []string{"image/jpeg", "image/jpg", "image/gif", "image/png"}

func (self Config) Upload(bucket, path string, images ...*multipart.FileHeader) ([]string, error) {
	if bucket == "" || path == "" {
		return nil, errors.UnknownData()
	}
	urls := make([]string, len(images))

	for i, image := range images {
		file, _ := image.Open()
		buff := make([]byte, 512)
		if _, err := file.Read(buff); err != nil {
			return nil, err
		}

		if !contains(types, http.DetectContentType(buff)) {
			return nil, errors.InvalidRequestData()
		}
		img := strings.Split(image.Filename, ".")
		name := strconv.FormatInt(time.Now().UnixNano(), 10) + "." + img[len(img)-1]
		destination := path + name
		up, err := self.s3Uploader.Upload(&s3manager.UploadInput{
			Bucket: aws.String(bucket),
			Key:    aws.String(destination),
			Body:   file,
		})
		if err != nil {
			return nil, err
		}

		urls[i] = up.Location
	}

	return urls, nil

}

func (self Config) DeleteItems(bucket string, keys ...string) {
	for _, v := range keys {
		_, err := self.s3Client.DeleteObject(&s3.DeleteObjectInput{Bucket: aws.String(bucket), Key: aws.String(v)})
		if err != nil {
			fmt.Printf("Unable to delete object %q from bucket %q, %v", v, bucket, err)
		}
	}
}

func (self Config) SendHTMLEmailSes(input MailInput) error {
	toAddresses := make([]*string, len(input.To))
	for k, v := range input.To {
		toAddresses[k] = aws.String(v)
	}
	_, err := self.sesClient.SendEmail(&ses.SendEmailInput{
		Destination: &ses.Destination{
			CcAddresses: []*string{},
			ToAddresses: toAddresses,
		},
		Message: &ses.Message{
			Body: &ses.Body{
				Html: &ses.Content{
					Charset: aws.String(CHARSET),
					Data:    aws.String(input.HtmlText),
				},
			},
			Subject: &ses.Content{
				Charset: aws.String(CHARSET),
				Data:    aws.String(input.Subject),
			},
		},
		Source: aws.String(input.From),
	})
	return err
}

func contains(elems []string, v string) bool {
	for _, s := range elems {
		if v == s {
			return true
		}
	}
	return false
}
