package context

import "context"

type KeyReference string

const (
	AccountIdKey         = KeyReference("AccountId")
	ActivationCodeKey    = KeyReference("ActivationCode")
	LanguageKey          = KeyReference("Language")
	OfficeIdKey          = KeyReference("OfficeId")
	ResourceIdKey        = KeyReference("ResourceId")
	ServiceIdKey         = KeyReference("ServiceId")
	TokenKey             = KeyReference("TokenKey")
	UserIdKey            = KeyReference("UserId")
	UserNameKey          = KeyReference("UserName")
	AdminUserKey         = KeyReference("AdminUser")
	EmailValidationRegex = "^[a-zA-Z0-9.!#$%&'*+\\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$"
)

type Values struct {
	ActivationCode string
	AccountId      string
	Language       string
	OfficeId       string
	ResourceId     string
	ServiceId      string
	Token          string
	UserId         string
	UserName       string
	AdminUser      bool
}

func ContextKeys(ctx context.Context) (data Values) {
	if ctx == nil {
		return
	}

	if rawCode, ok := ctx.Value(ActivationCodeKey).(string); ok {
		data.ActivationCode = rawCode
	}

	if rawAccountId, ok := ctx.Value(AccountIdKey).(string); ok {
		data.AccountId = rawAccountId
	}

	if rawLang, ok := ctx.Value(LanguageKey).(string); ok {
		data.Language = rawLang
	}

	if rawOfficeId, ok := ctx.Value(OfficeIdKey).(string); ok {
		data.OfficeId = rawOfficeId
	}

	if rawResourceId, ok := ctx.Value(ResourceIdKey).(string); ok {
		data.ResourceId = rawResourceId
	}

	if rawServiceId, ok := ctx.Value(ServiceIdKey).(string); ok {
		data.ServiceId = rawServiceId
	}

	if rawToken, ok := ctx.Value(TokenKey).(string); ok {
		data.Token = rawToken
	}

	if rawUserId, ok := ctx.Value(UserIdKey).(string); ok {
		data.UserId = rawUserId
	}

	if rawUserName, ok := ctx.Value(UserNameKey).(string); ok {
		data.UserName = rawUserName
	}

	if ad, ok := ctx.Value(AdminUserKey).(bool); ok {
		data.AdminUser = ad
	}

	return
}
