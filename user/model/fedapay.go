package model

import "time"

type PayCustomer struct {
	Email       string      `json:"email"`
	FirstName   string      `json:"firstname"`
	LastName    string      `json:"lastname"`
	PhoneNumber PhoneNumber `json:"phone_number,omitempty"`
}

type PhoneNumber struct {
	Number  string `json:"number,omitempty"`
	Country string `json:"country,omitempty"`
}

type PayCurrency struct {
	Iso string `json:"iso"`
}

type PayTransactionRequest struct {
	Description string      `json:"description"`
	Amount      float32     `json:"amount"`
	Currency    PayCurrency `json:"currency"`
	Customer    PayCustomer `json:"customer"`
	CallBackUrl string      `json:"callback_url"`
}

type PayTransaction struct {
	Id         int       `json:"id"`
	CreatedAt  time.Time `json:"created_at"`
	Status     string    `json:"status"`
	ApprovedAt time.Time `json:"approved_at"`
	Amount     float32   `json:"amount"`
}

type PayToken struct {
	Token string `json:"token"`
	Url   string `json:"url"`
	//Currency    PayCurrency `json:"currency"`
	//Customer    PayCustomer `json:"customer"`
}
