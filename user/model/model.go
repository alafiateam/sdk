package model

import "time"

type User struct {
	ID             string     `json:"id"`
	Email          string     `json:"email"`
	FirstName      *string    `json:"firstName"`
	LastName       *string    `json:"lastName"`
	IsActive       bool       `json:"isActive"`
	PhoneNumber    *string    `json:"phoneNumber"`
	WhatsappNumber *string    `json:"whatsappNumber"`
	PhotoUrl       *string    `json:"photoUrl"`
	Password       string     `json:"password"`
	Address        *string    `json:"address"`
	TownId         uint       `json:"townId"`
	SubExpireAt    *time.Time `json:"sub_expire_at"`
	Token          string     `json:"token"`
	Code           string     `json:"code"`
}

type SubOption struct {
	ID       uint    `json:"id"`
	Name     string  `json:"name"`
	Value    float32 `json:"value"`
	Duration uint    `json:"duration"`
}

type Subscription struct {
	ID            uint       `json:"id"`
	TransactionID string     `json:"transactionId"`
	Extras        string     `json:"extras"`
	Value         float32    `json:"value"`
	Duration      uint       `json:"duration"`
	OptionID      uint       `json:"optionId"`
	CreatedAt     *time.Time `json:"createdAt"`
	Currency      string     `json:"currency"`
}

type SubState struct {
	ExpireAt    time.Time `json:"expireAt"`
	IsPremium   bool      `json:"isPremium"`
	CanPublish  bool      `json:"canPublish"`
	PhotoNumber int       `json:"photoNumber"`
}

const (
	APPROVED    = "approved"
	PENDING     = "pending"
	DECLINED    = "declined"
	CANCELED    = "canceled"
	REFUNDED    = "refunded"
	TRANSFERRED = "transferred"
)
