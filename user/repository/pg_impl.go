package repository

import (
	_ "database/sql"
	"log"
	"time"

	"github.com/google/uuid"
	"gitlab.com/alafiateam/sdk/errors"
)

func (r repository) Active(userId uint) *errors.Error {
	if userId == 0 {
		return errors.InvalidRequestData()
	}
	_, err := r.db.Exec(activeUser, userId)
	return errors.DBError(err)
}

func (r repository) ChangePassword(userId uint, email, newPassword string) *errors.Error {
	if (userId == 0 && email == "") || len(newPassword) == 0 {
		return errors.InvalidRequestData()
	}
	res, err := r.db.Exec(changePassword, userId, email, newPassword)
	if err != nil {
		return errors.DBError(err)
	}
	if n, e := res.RowsAffected(); n != 1 {
		return errors.DBError(e)
	}
	return nil
}

func (r repository) Delete(email string) *errors.Error {

	res, err := r.db.Exec("UPDATE common.users SET deleted_at=now() WHERE email=$1", email)
	if err != nil {
		return errors.DBError(err)
	}
	if n, e := res.RowsAffected(); n != 1 {
		return errors.DBError(e)
	}
	return nil
}

func (r repository) ChangeEmail(userId uint, email, activationCode string) *errors.Error {
	if userId == 0 || len(email) == 0 {
		return errors.InvalidRequestData()
	}
	_, err := r.db.Exec(changeEmail, userId, email, activationCode)
	return errors.DBError(err)
}

func (r repository) CreateUser(request Users) (string, *errors.Error) {
	row, err := r.db.NamedQuery(insertNewUser, request)
	if err != nil {
		return "", errors.DBError(err)
	}
	var ID uuid.UUID
	if row.Next() {
		row.Scan(&ID)
	}

	return ID.String(), errors.DBError(err)
}

func (r repository) EndSession(sessionId string) {
	return
}

func (r repository) InsertActivationCode(userId uint, code string) *errors.Error {
	if userId == 0 {
		return errors.InvalidRequestData()
	}
	_, err := r.db.Exec(activationCode, userId, code)
	return errors.DBError(err)
}

func (r repository) Login(email string) Entity {
	var result []Entity
	if err := r.db.Select(&result, userByName, email); err != nil {
		log.Println(err)
		return Entity{}
	}
	if len(result) == 0 {
		return Entity{}
	}
	return result[0]
}

func (r repository) StartSession(sessionId string) *errors.Error {
	return nil
}

func (r repository) UserByID(userId uint, email string) (Entity, *errors.Error) {
	var result []Entity
	if err := r.db.Select(&result, UserByID, userId, email); err != nil {
		log.Println(err)
		return Entity{}, errors.DBError(err)
	}
	if len(result) == 0 {
		return Entity{}, nil
	}
	return result[0], nil
}

/*
func (r repository) Users(resourceId string) ([]UserResult, *errors.Error) {
	var result []UserResult
	var err error
	if resourceId != "" {
		err = r.db.Select(&result, usersByResource, resourceId)
	} else {
		err = r.db.Select(&result, usersWithoutResource)
	}

	return result, errors.DBError(err)
}
*/

func (r repository) ChangeUserInfo(request Users) *errors.Error {
	_, err := r.db.NamedExec(changeUser, request)
	return errors.DBError(err)
}

func (r repository) UpdateProfileImage(userId uint, url string) *errors.Error {
	_, err := r.db.Exec(profileImage, userId, url)
	return errors.DBError(err)
}

func (r repository) InsertCode(email, code string, typ int) *errors.Error {
	_, err := r.db.Exec(insertCode, email, code, typ)
	return errors.DBError(err)
}

func (r repository) GetCode(email string, d time.Time, typ int) (int, string) {

	row := r.db.QueryRow(getCode, email, d, typ)
	var code string
	var id int
	_ = row.Scan(&id, &code)
	return id, code
}

func (r repository) GetCreationCode(email string, typ int) (int, string) {
	row := r.db.QueryRow(getCreationCode, email, typ)
	var code string
	var id int
	_ = row.Scan(&id, &code)
	return id, code
}

func (r repository) UpdateRecoveryCode(id int) *errors.Error {
	_, err := r.db.Exec(updateRecoveryCode, id)
	return errors.DBError(err)
}
