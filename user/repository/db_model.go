package repository

import (
	"time"
)

type Entity struct {
	ID             uint      `db:"id"`
	Email          string    `db:"email"`
	FirstName      *string   `db:"first_name"`
	LastName       *string   `db:"last_name"`
	PhoneNumber    *string   `db:"phone_number"`
	WhatsappNumber *string   `db:"whatsapp_number"`
	Password       string    `db:"password"`
	ActivationCode *string   `db:"activation_code"`
	IsActive       bool      `db:"active"`
	PhotoUrl       *string   `db:"photo_url"`
	SubExpireAt    time.Time `db:"sub_expire_at"`
	LastPublish    time.Time `db:"last_publish"`
	TownId         uint      `db:"town_id"`
	Address        *string   `db:"address"`
}

type Users struct {
	ID             uint
	Email          string
	FirstName      *string
	LastName       *string
	PhoneNumber    *string
	WhatsappNumber *string
	Password       string
	ActivationCode string
	IsActive       bool
	PhotoUrl       *string
	SubExpireAt    *time.Time
	TownId         uint
	Address        *string
}

type Subscription struct {
	ID            uint
	OptionID      uint `gorm:"index" db:"option_id"`
	Value         float32
	Duration      uint
	UserID        uint   `gorm:"index" db:"user_id"`
	TransactionID string `db:"transaction_id"`
	Extras        string
	CreatedAt     *time.Time `db:"created_at"`
	ValidatedAt   *time.Time `db:"validated_at"`
	Currency      string
}

type subscriptionOption struct {
	ID       uint `gorm:"primarykey"`
	Name     string
	Value    float32
	Duration uint
	Valid    bool `gorm:"default:true"`
	Currency string
}
