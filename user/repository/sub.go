package repository

import (
	"log"
	"time"

	"github.com/google/uuid"
	"gitlab.com/alafiateam/sdk/errors"
)

func (r repository) FindSubscriptionOptions() ([]subscriptionOption, *errors.Error) {
	var result []subscriptionOption
	err := r.db.Select(&result, "select * from common.subscription_options")
	return result, errors.DBError(err)
}

func (r repository) SaveSubscription(sub Subscription) (string, *errors.Error) {
	row, err := r.db.NamedQuery(insertSub, sub)
	if err != nil {
		return "", errors.DBError(err)
	}
	var ID uuid.UUID
	if row.Next() {
		row.Scan(&ID)
	}

	return ID.String(), errors.DBError(err)
}

func (r repository) SubOption(ID uint) (subscriptionOption, *errors.Error) {
	var result []subscriptionOption
	if err := r.db.Select(&result, "select * from common.subscription_options where id=$1", ID); err != nil {
		log.Println(err)
		return subscriptionOption{}, errors.DBError(err)
	}
	if len(result) == 0 {
		return subscriptionOption{}, nil
	}
	return result[0], nil
}

func (r repository) AddValidity(userID uint, date time.Time) *errors.Error {
	_, err := r.db.Exec(addValidity, userID, date)
	return errors.DBError(err)
}

func (r repository) GetMySubs(userID uint) ([]Subscription, *errors.Error) {
	var subs []Subscription
	err := r.db.Select(&subs, "select * from common.subscriptions where user_id=$1 AND validated_at IS NOT NULL ", userID)
	return subs, errors.DBError(err)
}

func (r repository) FindSubscriptionByTransaction(transactionId int) (Subscription, *errors.Error) {
	var subs []Subscription
	err := r.db.Select(&subs, "select * from common.subscriptions where transaction_id=$1", transactionId)
	return subs[0], errors.DBError(err)
}
func (r repository) UpdateSubscription(id uint, description string) *errors.Error {
	_, err := r.db.Exec(updateSubscription, id, description)
	return errors.DBError(err)
}
