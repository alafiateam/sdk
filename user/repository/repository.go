package repository

import (
	"time"

	"gitlab.com/alafiateam/sdk/connector/pgsql"
	"gitlab.com/alafiateam/sdk/errors"
	_ "gitlab.com/alafiateam/sdk/user/model"
)

type UserRepository interface {
	Active(userId uint) *errors.Error

	InsertActivationCode(userId uint, code string) *errors.Error

	ChangePassword(userId uint, email, newPassword string) *errors.Error

	CreateUser(request Users) (string, *errors.Error)

	ChangeUserInfo(request Users) *errors.Error

	EndSession(sessionId string)

	Login(email string) Entity

	UserByID(userId uint, email string) (Entity, *errors.Error)

	InsertCode(email, code string, typ int) *errors.Error

	UpdateProfileImage(userId uint, url string) *errors.Error

	ChangeEmail(userId uint, email, activationCode string) *errors.Error

	FindSubscriptionOptions() ([]subscriptionOption, *errors.Error)

	SubOption(ID uint) (subscriptionOption, *errors.Error)

	SaveSubscription(sub Subscription) (string, *errors.Error)

	AddValidity(userID uint, date time.Time) *errors.Error

	GetMySubs(userID uint) ([]Subscription, *errors.Error)

	GetCode(email string, d time.Time, typ int) (int, string)

	UpdateRecoveryCode(id int) *errors.Error

	FindSubscriptionByTransaction(transactionId int) (Subscription, *errors.Error)

	UpdateSubscription(id uint, description string) *errors.Error

	Delete(email string) *errors.Error

	GetCreationCode(email string, typ int) (int, string)
}

type repository struct {
	db *pgsql.DB
}

func NewUserRepository(db *pgsql.DB) UserRepository {
	return repository{db: db}
}
