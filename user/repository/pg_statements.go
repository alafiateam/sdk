package repository

const (
	insertNewUser = `INSERT INTO common.users(
							email,
							phone_number,
							whatsapp_number,
							password ,
							first_name,
							last_name,
							address,
							town_id,
							sub_expire_at)
					VALUES(	:email,
							:phonenumber,
							:whatsappnumber,
							:password ,
							:firstname,
							:lastname,
							:address,
							:townid,
							:subexpireat)
					RETURNING id`

	userByName = `SELECT id, password, first_name, last_name, active, photo_url, phone_number, sub_expire_at, address, town_id FROM common.users WHERE email = $1 AND deleted_at IS NULL `

	UserByID = `SELECT id, email, first_name, last_name, password, activation_code, active, phone_number, whatsapp_number, photo_url, sub_expire_at, address, town_id FROM common.users WHERE (id = $1 OR email=$2) AND deleted_at IS NULL`

	changePassword = `UPDATE common.users SET password=$3 WHERE (id=$1 OR email=$2) AND deleted_at IS NULL`

	changeEmail = `UPDATE common.users SET  email=$2, activation_code=$3, active=false WHERE id=$1`

	activeUser = `UPDATE common.users SET  active=true WHERE id=$1`

	activationCode = `UPDATE common.users SET  activation_code=$2, active=false WHERE id=$1`

	profileImage = `UPDATE common.users SET  photo_url=$2 WHERE id=$1`

	changeUser = `UPDATE common.users SET phone_number = :phonenumber, whatsapp_number = :whatsappnumber, first_name = :firstname, last_name = :lastname, address=:address, town_id=:townid
				  WHERE id = :id`
	insertSub = `INSERT INTO common.subscriptions(
							option_id,
							value,
							duration ,
							user_id,
							transaction_id,
							extras,
							currency)
					VALUES(	:option_id,
							:value,
							:duration ,
							:user_id,
							:transaction_id,
							:extras,
							:currency)
					RETURNING id`
	addValidity = `UPDATE common.users SET sub_expire_at=$2 WHERE id=$1`

	insertCode = `INSERT INTO common.recovery(email,code,type) VALUES($1,$2,$3)`

	getCode = `SELECT id, code FROM common.recovery 
						WHERE email=$1 AND validated_at IS NULL 
						AND created_at >= $2 AND type = $3 
						order by created_at DESC limit 1`

	getCreationCode = `SELECT id, code FROM common.recovery 
						WHERE email=$1 AND validated_at IS NULL AND type = $2 
						order by created_at DESC limit 1`

	updateRecoveryCode = `UPDATE common.recovery SET  validated_at=now() WHERE id=$1`

	updateSubscription = `UPDATE common.subscriptions SET extras=$2, validated_at=now() WHERE id=$1`
)
