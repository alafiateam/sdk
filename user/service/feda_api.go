package service

import (
	"bytes"
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"strconv"

	"gitlab.com/alafiateam/sdk/user/model"
)

func createTransaction(transactionReq model.PayTransactionRequest) (model.PayTransaction, error) {
	data, err := json.Marshal(transactionReq)
	if err != nil {
		return model.PayTransaction{}, err
	}
	request, err := http.NewRequest("POST", os.Getenv("FEDA_TRANSACTION_URL"), bytes.NewReader(data))
	if err != nil {
		return model.PayTransaction{}, err
	}
	body, err := makeCall(request)
	if err != nil {
		return model.PayTransaction{}, err
	}

	response := make(map[string]model.PayTransaction)
	err2 := json.Unmarshal(body, &response)
	if err2 != nil {
		return model.PayTransaction{}, nil
	}
	transaction := model.PayTransaction{}
	for _, v := range response {
		if v.Id != 0 && !v.CreatedAt.IsZero() {
			transaction = v
			break
		}
	}

	return transaction, nil
}

func createPayLink(transactionId int) (string, error) {
	url := os.Getenv("FEDA_TRANSACTION_URL") + strconv.Itoa(transactionId) + "/token"
	request, err := http.NewRequest("POST", url, nil)
	if err != nil {
		return "", err
	}
	body, err := makeCall(request)
	response := model.PayToken{}
	err = json.Unmarshal(body, &response)
	if err != nil {
		log.Println(err)
		return "", nil
	}

	return response.Url, nil
}

func getTransaction(transactionId int) (model.PayTransaction, string) {
	url := os.Getenv("FEDA_TRANSACTION_URL") + strconv.Itoa(transactionId)
	request, err := http.NewRequest("GET", url, nil)
	if err != nil {
		return model.PayTransaction{}, ""
	}
	body, err := makeCall(request)
	response := make(map[string]model.PayTransaction)
	err = json.Unmarshal(body, &response)
	if err != nil {
		return model.PayTransaction{}, ""
	}
	transaction := model.PayTransaction{}
	for _, v := range response {
		if v.Id != 0 && !v.CreatedAt.IsZero() {
			transaction = v
			break
		}
	}
	return transaction, string(body)
}

func makeCall(request *http.Request) ([]byte, error) {
	bearer := os.Getenv("FEDA_TOKEN")
	request.Header.Add("Authorization", bearer)
	request.Header.Add("Content-Type", "application/json")
	client := &http.Client{}
	resp, err := client.Do(request)
	if err != nil {
		return nil, err
	}
	defer resp.Body.Close()
	if resp.StatusCode != 200 {
		b, _ := ioutil.ReadAll(resp.Body)
		return nil, errors.New(string(b))
	}
	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return nil, err
	}
	return body, nil
}
