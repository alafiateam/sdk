package service

import (
	"context"

	"gitlab.com/alafiateam/sdk/errors"
	"gitlab.com/alafiateam/sdk/user/model"
	"gitlab.com/alafiateam/sdk/user/repository"
)

//the context may have all needed keys to perform operation
//see context package for keys

//function are sorted alphabetically
type Service interface {
	//active current user
	//activation code must be sent by the context
	Active(ctx context.Context) *errors.Error

	//change the current user password
	ChangePassword(ctx context.Context, oldPassword, newPassword string) *errors.Error

	//user creation
	//user id and activation code are returned
	Create(request model.User) (string, *errors.Error)

	//change uer info
	UpdateInfo(ctx context.Context, request model.User) (model.User, *errors.Error)

	//user login
	Login(email, password string) (string, *errors.Error)

	//user logout
	Logout(ctx context.Context)

	//generate a new activation code to the user
	GenerateCode(userID string) ([3]string, *errors.Error)

	//get user info
	GetUser(userID string) (model.User, *errors.Error)

	//retrieve users list of a resource
	//Accounts(ctx context.Context) ([]model.Account, *errors.Error)

	ForgotPassword(email string) (string, string, *errors.Error)

	UpdateProfileImage(ctx context.Context, url string) (model.User, *string, *errors.Error)

	EmailChange(userId, email string) (string, string, *errors.Error)

	SubscriptionOptions() ([]model.SubOption, *errors.Error)

	CreateSubscription(sub model.Subscription, userID string) (string, *errors.Error)

	UserSubs(userID string) []model.Subscription

	UserSubState(userID string) model.SubState

	RecoveryPassword(email, code, password string) *errors.Error

	//start user creation
	StartCreate(email string) (string, string, *errors.Error)

	FedaPayResponse(transactionId string) (bool, string)

	StartDelete(userID string) (string, string, string, *errors.Error)

	EndDelete(userID, code string) *errors.Error
}

type service struct {
	repository repository.UserRepository
}

func NewAccountService(repo repository.UserRepository) Service {
	return InitWithEngine(repo)
}

func InitWithEngine(repo repository.UserRepository) Service {
	return &service{
		repository: repo,
	}
}
