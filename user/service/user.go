package service

import (
	"context"
	"log"
	"os"
	"strconv"
	"strings"
	"time"

	appContext "gitlab.com/alafiateam/sdk/context"
	"gitlab.com/alafiateam/sdk/errors"
	"gitlab.com/alafiateam/sdk/jwt"
	"gitlab.com/alafiateam/sdk/user/model"
	"gitlab.com/alafiateam/sdk/user/repository"
	"gitlab.com/alafiateam/sdk/utils"
	"golang.org/x/crypto/bcrypt"
)

const (
	recoveryDelay = 60
)

func (self *service) Active(ctx context.Context) *errors.Error {
	ctxValue := appContext.ContextKeys(ctx)
	result, err := self.repository.UserByID(utils.ID2Int(ctxValue.UserId), ctxValue.UserId)
	if err != nil {
		return err
	}
	if err := comparePassword(*result.ActivationCode, ctxValue.ActivationCode); err != nil {
		return errors.DBError(err)
	}
	return self.repository.Active(utils.ID2Int(ctxValue.UserId))
}

func (self *service) ChangePassword(ctx context.Context, oldPassword, newPassword string) *errors.Error {
	if oldPassword == "" || newPassword == "" {
		return errors.InvalidRequestData()
	}
	ctxValue := appContext.ContextKeys(ctx)
	result, err := self.repository.UserByID(utils.ID2Int(ctxValue.UserId), ctxValue.UserId)
	if err != nil {
		return err
	}
	if err := comparePassword(result.Password, oldPassword); err != nil {
		return errors.DBError(err)
	}
	hashNewPass, _ := utils.HashPassword(newPassword)
	return self.repository.ChangePassword(utils.ID2Int(ctxValue.UserId), "", hashNewPass)
}

func (self *service) Create(request model.User) (string, *errors.Error) {
	rq, er := jwt.ParseToken(request.Token)
	if er != nil {
		return "", errors.InvalidValidToken()
	}

	if rq.UserId != utils.ID2Int(request.Code) {
		return "", errors.InvalidRequestData()
	}

	hashPass, _ := utils.HashPassword(request.Password)
	query := repository.Users{
		Email:          strings.ToLower(rq.UserName),
		Password:       hashPass,
		PhoneNumber:    request.PhoneNumber,
		WhatsappNumber: request.WhatsappNumber,
		FirstName:      request.FirstName,
		LastName:       request.LastName,
		Address:        request.Address,
		SubExpireAt:    request.SubExpireAt,
	}

	createdId, err := self.repository.CreateUser(query)
	if err != nil {
		return "", err
	}
	id, _ := self.repository.GetCreationCode(strings.ToLower(rq.UserName), 3)
	if err := self.repository.UpdateRecoveryCode(id); err != nil {
		log.Println(err)
	}
	/*
		if self.mailEngine != nil {
			_, err = self.mailEngine.ActivationMessage(request.LastName, request.Email, activationCode, request.Language)
			if err != nil {
				log.Println(err)
			}
		}
	*/
	return createdId, nil
}

func (self *service) UpdateInfo(ctx context.Context, request model.User) (model.User, *errors.Error) {
	ctxValue := appContext.ContextKeys(ctx)

	query := repository.Users{
		Email:          request.Email,
		PhoneNumber:    request.PhoneNumber,
		WhatsappNumber: request.WhatsappNumber,
		FirstName:      request.FirstName,
		LastName:       request.LastName,
		ID:             utils.ID2Int(ctxValue.UserId),
		Address:        request.Address,
		TownId:         request.TownId,
	}
	if err := self.repository.ChangeUserInfo(query); err != nil {
		return model.User{}, errors.InvalidRequestData()
	}
	return self.accountInfo(ctxValue.UserId)
}

func (self *service) Login(email, password string) (string, *errors.Error) {

	result := self.repository.Login(strings.ToLower(email))

	if comparePassword(result.Password, password) != nil {
		return "", errors.Unauthorized()
	}

	tokenString, err := jwt.GenerateToken(result.ID, false, result.IsActive, email, "")
	if err != nil {
		return "", errors.Unknown()
	}
	return tokenString, nil
	//if err = self.repository.StartSession(tokenString); err != nil {
	//	return model.LoginResponse{}, errors.InvalidRequestData()
	//}
}

func (self *service) Logout(ctx context.Context) {
	ctxValues := appContext.ContextKeys(ctx)
	self.repository.EndSession(ctxValues.Token)
}

func (self *service) GenerateCode(userID string) ([3]string, *errors.Error) {
	ret := [3]string{}
	result, _ := self.repository.UserByID(utils.ID2Int(userID), userID)
	if len(result.Email) == 0 {
		return ret, errors.Unknown()
	}
	activationCode := strconv.Itoa(utils.GenerateCode(false))
	hashCode, _ := utils.HashPassword(activationCode)
	if err := self.repository.InsertActivationCode(utils.ID2Int(userID), hashCode); err != nil {
		return ret, err
	}
	ret[0] = result.Email
	ret[1] = *result.FirstName
	ret[2] = activationCode

	return ret, nil

}

func (self *service) GetUser(userID string) (model.User, *errors.Error) {
	return self.accountInfo(userID)
}

func (self *service) accountInfo(ID string) (model.User, *errors.Error) {
	v, err := self.repository.UserByID(utils.ID2Int(ID), ID)
	if err != nil {
		return model.User{}, err
	}
	if v.Email == "" {
		return model.User{}, nil
	}
	return model.User{
		ID:             strconv.Itoa(int(v.ID)),
		Email:          v.Email,
		FirstName:      v.FirstName,
		LastName:       v.LastName,
		IsActive:       v.IsActive,
		PhoneNumber:    v.PhoneNumber,
		WhatsappNumber: v.WhatsappNumber,
		PhotoUrl:       v.PhotoUrl,
	}, nil
}

func comparePassword(hashedPassword, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

func (self *service) EmailChange(userId, email string) (string, string, *errors.Error) {
	result, err := self.repository.UserByID(utils.ID2Int(userId), userId)
	if err != nil || result.Email == "" || result.Email == email {
		log.Println(err)
		return "", "", errors.Unauthorized()
	}
	activationCode := strconv.Itoa(utils.GenerateCode(false))
	hashCode, _ := utils.HashPassword(activationCode)
	if err = self.repository.ChangeEmail(utils.ID2Int(userId), email, hashCode); err != nil {
		log.Println(err)
		return "", "", errors.UnknownData()
	}

	return activationCode, *result.FirstName, nil
}

func (self *service) UpdateProfileImage(ctx context.Context, url string) (model.User, *string, *errors.Error) {
	ctxValue := appContext.ContextKeys(ctx)
	u, _ := self.accountInfo(ctxValue.UserId)
	old := u.PhotoUrl
	err := self.repository.UpdateProfileImage(utils.ID2Int(ctxValue.UserId), url)
	if err != nil {
		return model.User{}, nil, err
	}
	u, err = self.accountInfo(ctxValue.UserId)
	return u, old, err
}

func (self *service) ForgotPassword(email string) (string, string, *errors.Error) {
	result, _ := self.repository.UserByID(0, email)
	if result.Email != email {
		return "", "", errors.ForgotPassword()
	}

	code := self.genCode(result.Email, 1)
	if code == "" {
		return "", "", errors.Unauthorized()
	}

	return code, *result.FirstName, nil
}

func (self *service) StartDelete(userID string) (string, string, string, *errors.Error) {
	result, _ := self.repository.UserByID(utils.ID2Int(userID), "")
	if result.ID != utils.ID2Int(userID) {
		return "", "", "", errors.Unauthorized()
	}

	code := self.genCode(result.Email, 2)
	if code == "" {
		return "", "", "", errors.Unauthorized()
	}

	return code, result.Email, *result.FirstName, nil
}

func (self *service) genCode(email string, typ int) string {

	code := strconv.Itoa(utils.GenerateCode(false))
	hashCode, _ := utils.HashPassword(code)

	err := self.repository.InsertCode(strings.ToLower(email), hashCode, typ)
	if err != nil {
		log.Println(err)
		return ""
	}

	return code
}

func (self *service) RecoveryPassword(email, code, password string) *errors.Error {
	delay := recoveryDelay
	if d, err := strconv.Atoi(os.Getenv("RECOVERY_DELAY")); err == nil {
		delay = d
	}
	id, hCode := self.repository.GetCode(strings.ToLower(email), time.Now().UTC().Add(-time.Minute*time.Duration(delay)), 1)

	if comparePassword(hCode, code) != nil {
		return errors.Unauthorized()
	}
	hashNewPass, _ := utils.HashPassword(password)
	if err := self.repository.ChangePassword(0, email, hashNewPass); err != nil {
		log.Println(err)
		return errors.Unauthorized()
	}
	if err := self.repository.UpdateRecoveryCode(id); err != nil {
		log.Println(err)
	}
	return nil
}

func (self *service) EndDelete(email, code string) *errors.Error {
	delay := recoveryDelay
	if d, err := strconv.Atoi(os.Getenv("RECOVERY_DELAY")); err == nil {
		delay = d
	}
	id, hCode := self.repository.GetCode(strings.ToLower(email), time.Now().UTC().Add(-time.Minute*time.Duration(delay)), 2)

	if comparePassword(hCode, code) != nil {
		return errors.Unauthorized()
	}
	if err := self.repository.Delete(email); err != nil {
		log.Println(err)
		return errors.Unauthorized()
	}
	if err := self.repository.UpdateRecoveryCode(id); err != nil {
		log.Println(err)
	}
	return nil
}

func (self *service) StartCreate(email string) (string, string, *errors.Error) {
	result := self.repository.Login(strings.ToLower(email))
	if result.ID != 0 {
		return "", "", errors.DuplicateUser()
	}
	_, c := self.repository.GetCreationCode(strings.ToLower(email), 3)
	code, e := strconv.Atoi(c)
	if e != nil {
		code = utils.GenerateCode(false)
		err := self.repository.InsertCode(strings.ToLower(email), strconv.Itoa(code), 3)
		if err != nil {
			log.Println(err)
		}
	}

	tokenString, err := jwt.GenerateToken(uint(code), false, false, email, "")
	if err != nil {
		return "", "", errors.Unknown()
	}
	return strconv.Itoa(code), tokenString, nil
}
