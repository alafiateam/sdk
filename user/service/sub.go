package service

import (
	"log"
	"os"
	"strconv"
	"time"

	"gitlab.com/alafiateam/sdk/errors"
	"gitlab.com/alafiateam/sdk/user/model"
	"gitlab.com/alafiateam/sdk/user/repository"
	"gitlab.com/alafiateam/sdk/utils"
)

const fedLogPrefix = "[FedaPay]: "

func (self *service) SubscriptionOptions() ([]model.SubOption, *errors.Error) {
	res, err := self.repository.FindSubscriptionOptions()
	if err != nil {
		return nil, err
	}
	r := make([]model.SubOption, len(res))
	for k, v := range res {
		r[k] = model.SubOption{
			ID:       v.ID,
			Name:     v.Name,
			Value:    v.Value,
			Duration: v.Duration,
		}
	}
	return r, nil
}

func (self *service) CreateSubscription(sub model.Subscription, userID string) (string, *errors.Error) {
	opt, err := self.repository.SubOption(sub.OptionID)
	if err != nil || opt.ID == 0 {
		log.Println("[subs]: ", err)
		return "", errors.Unknown()
	}
	user, err := self.repository.UserByID(utils.ID2Int(userID), userID)
	if err != nil {
		log.Println("[subs]: ", err)
		return "", errors.Unknown()
	}
	if !user.IsActive {
		return "", errors.Unauthorized()
	}

	customer := model.PayCustomer{
		Email:     user.Email,
		FirstName: *user.FirstName,
		LastName:  *user.LastName,
		//PhoneNumber: model.PhoneNumber{
		//	Number: *user.PhoneNumber,
		//	//Country: "tg",
		//},
	}

	transactionRequest := model.PayTransactionRequest{
		Description: "Souscription (" + strconv.Itoa(int(opt.Duration)) + " days) / Leger / Subscription (" + strconv.Itoa(int(opt.Duration)) + " jours)",
		Amount:      opt.Value,
		Currency:    model.PayCurrency{Iso: opt.Currency},
		Customer:    customer,
		CallBackUrl: os.Getenv("FEDA_CALLBACK_URL"),
	}
	transaction, er := createTransaction(transactionRequest)
	if er != nil {
		log.Println(fedLogPrefix, er)
		return "", errors.Unknown()
	}
	url, er := createPayLink(transaction.Id)
	if er != nil {
		log.Println(fedLogPrefix, er)
		return "", errors.Unknown()
	}

	req := repository.Subscription{
		OptionID:      opt.ID,
		Value:         opt.Value,
		Duration:      opt.Duration,
		UserID:        utils.ID2Int(userID),
		TransactionID: strconv.Itoa(transaction.Id),
		Extras:        sub.Extras,
		Currency:      opt.Currency,
	}
	_, err = self.repository.SaveSubscription(req)
	if err != nil {
		log.Println(fedLogPrefix, err)
		return "", errors.Unknown()
	}

	return url, nil
}

func (self *service) FedaPayResponse(transactionId string) (bool, string) {
	sub, err := self.repository.FindSubscriptionByTransaction(int(utils.ID2Int(transactionId)))
	if err != nil || sub.ID == 0 {
		log.Println(fedLogPrefix, err)
		return false, model.DECLINED
	}
	user, err := self.repository.UserByID(sub.UserID, strconv.Itoa(int(sub.UserID)))
	if err != nil {
		log.Println(fedLogPrefix, err)
		return false, model.DECLINED
	}
	transaction, body := getTransaction(int(utils.ID2Int(transactionId)))
	if transaction.Status != model.APPROVED || transaction.ApprovedAt.Before(transaction.CreatedAt) || transaction.Amount != sub.Value {
		return false, transaction.Status
	}
	d := time.Now().UTC()
	if user.SubExpireAt.After(d) {
		d = user.SubExpireAt
	}

	ex := d.AddDate(0, 0, int(sub.Duration))

	err = self.repository.AddValidity(sub.UserID, ex)
	if err != nil {
		log.Println(fedLogPrefix, err)
		return false, model.DECLINED
	}
	err = self.repository.UpdateSubscription(sub.ID, body)
	if err != nil {
		log.Println(fedLogPrefix, err)
		return false, model.DECLINED
	}
	return true, transaction.Status
}

func (self *service) UserSubs(userID string) []model.Subscription {
	res, err := self.repository.GetMySubs(utils.ID2Int(userID))
	if err != nil {
		log.Println("[subs] :", err)
		return nil
	}

	r := make([]model.Subscription, len(res))
	for k, v := range res {
		r[k] = model.Subscription{
			TransactionID: v.TransactionID,
			Extras:        v.Extras,
			Value:         v.Value,
			Duration:      v.Duration,
			ID:            v.ID,
			CreatedAt:     v.CreatedAt,
			Currency:      v.Currency,
		}
	}
	return r
}

func (self *service) UserSubState(userID string) (state model.SubState) {
	user, err := self.repository.UserByID(utils.ID2Int(userID), userID)
	if err != nil {
		log.Println("[subs] :", err)
		return
	}
	state.PhotoNumber = 5
	if s, ok := os.LookupEnv("FREEMIUM_PHOTO_NUMBER"); ok {
		if d, err := strconv.Atoi(s); err == nil {
			state.PhotoNumber = d
		}
	}
	state.ExpireAt = user.SubExpireAt
	if user.SubExpireAt.After(time.Now().UTC()) {
		state.IsPremium = true
		state.CanPublish = true
		state.PhotoNumber = 30
		if s, ok := os.LookupEnv("PREMIUM_PHOTO_NUMBER"); ok {
			if d, err := strconv.Atoi(s); err == nil {
				state.PhotoNumber = d
			}
		}
		return
	}
	dur := 30
	if s, ok := os.LookupEnv("FREEMIUM_DELAY"); ok {
		if d, err := strconv.Atoi(s); err == nil {
			dur = d
		}
	}
	if time.Now().UTC().Sub(user.LastPublish).Hours() > float64(dur*24) {
		state.CanPublish = true
	}
	return
}
