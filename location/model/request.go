package model

import "strconv"

type CountryRequest struct {
	ID        uint   `json:"id"`
	Indicator string `json:"indicator"`
	Name      string `json:"name"`
	IsoCode   string `json:"isoCode"`
}

func (c *CountryRequest) IsValid() bool {
	ind, err := strconv.Atoi(c.Indicator)
	if err != nil {
		return false
	}
	return ind > 0 && len(c.Name) > 0 && len(c.IsoCode) > 0
}

func (c *CountryRequest) Ind() int {
	ind, _ := strconv.Atoi(c.Indicator)
	return ind
}

type TownRequest struct {
	ID        string  `json:"id,omitempty"`
	Name      string  `json:"name"`
	Longitude float64 `json:"longitude"`
	Latitude  float64 `json:"latitude"`
	CountryID uint    `json:"countryId"`
}

func (c *TownRequest) IsValid() bool {
	return len(c.Name) > 0 && c.CountryID > 0
}

type DistrictRequest struct {
	ID     string `json:"id"`
	Name   string `json:"name"`
	TownID uint   `json:"townId"`
}

func (c *DistrictRequest) IsValid() bool {
	return c.Name != "" && c.TownID > 0
}
