package model

type Country struct {
	ID        uint   `json:"id"`
	Indicator string `json:"indicator"`
	Name      string `json:"name"`
	IsoCode   string `json:"isoCode"`
	Towns     []Town `json:"towns"`
}

type Town struct {
	ID          uint    `json:"id"`
	Name        string  `json:"name"`
	Longitude   float64 `json:"longitude"`
	Latitude    float64 `json:"latitude"`
	CountryID   uint    `json:"countryId"`
	CountryName string  `json:"countryName"`
}

type District struct {
	ID       uint   `json:"id"`
	Name     string `json:"name"`
	TownID   uint   `json:"townId"`
	TownName string `json:"townName"`
}

type Location struct {
	ID       uint   `json:"id"`
	Location string `json:"location"`
	IsoCode  string `json:"isoCode"`
}
