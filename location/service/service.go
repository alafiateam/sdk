package service

import (
	"gitlab.com/alafiateam/sdk/errors"
	"gitlab.com/alafiateam/sdk/location/model"
	"gitlab.com/alafiateam/sdk/location/repository"
)

type LocationService interface {
	UpsertCountry(request model.CountryRequest) (string, *errors.Error)

	UpsertTown(request model.TownRequest) (string, *errors.Error)

	UpsertDistrict(request model.DistrictRequest) (string, *errors.Error)

	Countries() ([]model.Country, *errors.Error)

	Towns() ([]model.Town, *errors.Error)

	TownsByCountry(countryID string) ([]model.Town, *errors.Error)

	DistrictsByTown(townID string) ([]model.District, *errors.Error)

	Districts() ([]model.District, *errors.Error)

	LocationByDistrict() ([]model.Location, *errors.Error)

	LocationByTown() ([]model.Location, *errors.Error)
}

type locationService struct {
	repository repository.LocationRepository
}

func NewLocationService(repo repository.LocationRepository) LocationService {
	return &locationService{repository: repo}
}
