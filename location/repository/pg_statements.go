package repository

const (
	insertNewCountry = `INSERT INTO common.countries(indicator, name, iso_code)
					VALUES(:indicator, :name, :iso_code) RETURNING id`

	updateCountry = `UPDATE common.countries set indicator = :indicator, name = :name, iso_code = :iso_code WHERE id = :id RETURNING id`

	insertNewTown = `INSERT INTO common.towns(name, country_id, longitude, latitude)
					VALUES(:name, :countryid, :longitude, :latitude) 
					RETURNING id`

	updateTown = `UPDATE common.towns set name = :name, country_id = :countryid, longitude = :longitude, latitude = :latitude
					WHERE id = :id
					RETURNING id`

	insertNewDistrict = `INSERT INTO common.districts(name, town_id) VALUES(:name, :townid) RETURNING id`

	updateDistrict = `UPDATE common.districts set name = :name, town_id = :townid where id = :id RETURNING id`

	getCountries = `SELECT * FROM common.countries`

	getTowns = `select t.id as "id", t.name as "name", t.longitude, t.latitude, t.country_id as "country_id", c.name as "country_name"
				from common.towns t
				inner join common.countries c on t.country_id = c.id`

	getTownsByCountry = `select t.id as "id", t.name as "name", t.longitude, t.latitude
				from common.towns t
				inner join common.countries c on t.country_id = c.id
				where c.id = $1 order by t.name`

	getDistricts = `select d.id as "id", d.name as "name", t.id as "town_id", t.name "town_name"
				from common.districts d
				inner join common.towns t on t.id = d.town_id`

	getDistrictsByTown = `select d.id as "id", d.name as "name", t.id as "town_id", t.name "town_name"
							from common.districts d
							inner join common.towns t on t.id = d.town_id where t.id = $1 
							order by t.name, d.name`

	getDistrictLocation = `SELECT d.id, c.iso_code, CONCAT(c.name, ',', t.name, ',', d.name) as location
						from common.districts d
						inner join common.towns t on t.id = d.town_id 
						inner join common.countries c on c.id = t.country_id 
						order by c.name, t.name, d.name`

	getTownLocation = `SELECT t.id, c.iso_code, CONCAT(t.name, ',', c.name) as location
						from common.towns t
						inner join common.countries c on c.id = t.country_id
						order by c.name, t.name`
)
