package repository

import (
	"gitlab.com/alafiateam/sdk/connector/pgsql"
	"gitlab.com/alafiateam/sdk/errors"
	"gitlab.com/alafiateam/sdk/location/model"
)

type LocationRepository interface {
	UpsertCountry(request Country) (string, *errors.Error)

	UpsertTown(request model.TownRequest) (string, *errors.Error)

	UpsertDistrict(request model.DistrictRequest) (string, *errors.Error)

	Countries() ([]Country, *errors.Error)
	
	Towns(countryID uint) ([]Town, *errors.Error)

	Districts(townID string) ([]District, *errors.Error)

	Localizations(byTown bool) ([]LocationResult, *errors.Error)
}

type repository struct {
	db *pgsql.DB
}


func NewLocationRepository(db *pgsql.DB) LocationRepository {
	return repository{db: db}
}
