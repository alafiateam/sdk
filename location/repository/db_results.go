package repository

import (
	"database/sql"
)

type Country struct {
	ID        uint    `db:"id"`
	Name      string  `db:"name"`
	IsoCode   string  `db:"iso_code"`
	Indicator int     `db:"indicator"`
	Currency  *string `db:"currency"`
}

type Town struct {
	ID          uint            `db:"id"`
	Name        string          `db:"name"`
	Longitude   sql.NullFloat64 `db:"longitude"`
	Latitude    sql.NullFloat64 `db:"latitude"`
	CountryID   uint            `db:"country_id"`
	CountryName string          `db:"country_name"`
}

type District struct {
	ID       uint   `db:"id"`
	Name     string `db:"name"`
	TownID   uint   `db:"town_id"`
	TownName string `db:"town_name"`
}

type LocationResult struct {
	ID       uint   `db:"id"`
	Location string `db:"location"`
	IsoCode  string `db:"iso_code"`
}
