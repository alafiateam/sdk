package utils

import (
	"math/rand"
	"strconv"
	"time"

	"gitlab.com/meeting-master/sdk/errors"
	"golang.org/x/crypto/bcrypt"
)

func HashPassword(password string) (string, *errors.Error) {
	bytes, err := bcrypt.GenerateFromPassword([]byte(password), bcrypt.DefaultCost)
	return string(bytes), errors.DBError(err)
}

func GenerateCode(max bool) int {
	rand.Seed(time.Now().UnixNano())
	if max {
		return 10000000 + rand.Intn(99999999-10000000)
	}
	return 10000 + rand.Intn(99999-10000)
}

func ID2Int(sid string) uint {
	if atoi, err := strconv.Atoi(sid); err == nil {
		return uint(atoi)
	}
	return 0
}