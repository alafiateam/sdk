package jwt

import (
	"errors"
	"os"
	"strconv"
	"time"

	"github.com/dgrijalva/jwt-go"
)

type AppClaims struct {
	UserId     uint   `json:"userId"`
	UserName   string `json:"userName"`
	ResourceId string `json:"resourceId"`
	IsAdmin    bool   `json:"isAdmin"`
	IsActive   bool   `json:"isActive"`
	jwt.StandardClaims
}

const (
	//tokenDuration = 8760
	expireOffset = 3600
)

var jwtKey = []byte(os.Getenv("TOKEN_KEY"))
var tokenDuration = 8760

// input userID as string
// output signed token and and error
// purpose: encrypt user information
func GenerateToken(userID uint, isAdmin, isActive bool, userName, ResourceId string) (tokenString string, err error) {
	if td, err := strconv.Atoi(os.Getenv("TOKEN_DURATION")); err == nil {
		tokenDuration = td
	}
	expirationTime := time.Now().Add(time.Duration(tokenDuration) * time.Hour)
	claims := &AppClaims{
		UserId:     userID,
		UserName:   userName,
		ResourceId: ResourceId,
		IsAdmin:    isAdmin,
		IsActive:   isActive,
		StandardClaims: jwt.StandardClaims{
			ExpiresAt: expirationTime.Unix(),
			IssuedAt:  time.Now().Unix(),
		},
	}
	// Declare the token with the algorithm used for signing, and the claims
	token := jwt.NewWithClaims(jwt.SigningMethodHS256, claims)
	// Create the JWT string
	tokenString, err = token.SignedString(jwtKey)
	return
}

// input token string
// output claims and error
// if parsing is ok (not expire and signature match) error will be nil
func ParseToken(tokenString string) (*AppClaims, error) {
	claims := &AppClaims{}
	tkn, err := jwt.ParseWithClaims(tokenString, claims, func(token *jwt.Token) (interface{}, error) {
		return jwtKey, nil
	})
	if err != nil {
		return nil, err
	}
	if !tkn.Valid {
		return nil, errors.New("invalid token")
	}
	return claims, nil
}
