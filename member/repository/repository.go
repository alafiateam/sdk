package repository

import (
	"gitlab.com/alafiateam/sdk/errors"
	"gorm.io/gorm"

)

type Repository interface {
	//User activation
	Active(ID uint) *errors.Error

	InsertActivationCode(ID uint, code string) *errors.Error

	ChangePassword(ID uint, newPassword string) *errors.Error

	Create(request Member) (string, *errors.Error)

	UpdateInfo(request Member) *errors.Error

	EndSession(sessionId string)

	//GrantUser(request model.GrantRequest) *errors.Error

	//Login(email string) Entity

	//StartSession(sessionId string) *errors.Error

	FindByID(ID uint, email string) (Member, *errors.Error)

	FindAll() ([]Member, *errors.Error)

	//InsertEndUser(ID, detail string)  *errors.Error

	//EndUserByID(ID string) (string, *errors.Error)
	//UpdateProfileImage(ID uint, url string) *errors.Error

	ChangeEmail(ID uint, email, activationCode string) *errors.Error
}

type repository struct {
	db *gorm.DB
}

func NewRepository(db *gorm.DB) (Repository, error) {
	err := db.AutoMigrate(&Member{})
	if err != nil {
		return repository{}, err
	}

	return repository{db: db}, nil
}
