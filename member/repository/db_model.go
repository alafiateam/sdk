package repository

import (
	"gorm.io/gorm"
)

//type Entity struct {
//	ID             uint    `db:"id"`
//	Email          string  `db:"email"`
//	FirstName      *string `db:"first_name"`
//	LastName       *string `db:"last_name"`
//	PhoneNumber    *string `db:"phone_number"`
//	Password       string  `db:"password"`
//	ActivationCode string  `db:"activation_code"`
//	IsActive       bool    `db:"active"`
//	IsAdmin        bool    `db:"admin"`
//	PhotoUrl       *string `db:"photo_url"`
//	TownId         uint    `db:"town_id"`
//	Address        *string `db:"address"`
//}

type Member struct {
	gorm.Model
	Email          string
	FirstName      *string
	LastName       *string
	PhoneNumber    *string
	Password       string
	ActivationCode string
	IsActive       bool
	IsAdmin        bool
	PhotoUrl       *string
	Address        *string
	CreatedBy      uint
}
