package repository

import (
	_ "database/sql"
	"strconv"

	"gitlab.com/alafiateam/sdk/errors"
)

func (r repository) Active(ID uint) *errors.Error {
	if ID != 0 {
		entity := Member{}
		entity.ID = ID
		res := r.db.Model(&entity).Update("active", true)
		return errors.DBError(res.Error)
	}
	return nil
}

func (r repository) ChangePassword(ID uint, newPassword string) *errors.Error {
	if ID == 0 || len(newPassword) == 0 {
		return errors.InvalidRequestData()
	}
	entity := Member{}
	entity.ID = ID
	res := r.db.Model(&entity).Update("password", newPassword)
	return errors.DBError(res.Error)
}

func (r repository) ChangeEmail(ID uint, email, activationCode string) *errors.Error {
	if ID == 0 || len(email) == 0 {
		return errors.InvalidRequestData()
	}
	entity := Member{}
	entity.ID = ID
	res := r.db.Model(&entity).Updates(Member{Email: email, ActivationCode: activationCode})
	return errors.DBError(res.Error)
}

func (r repository) Create(request Member) (string, *errors.Error) {
	res := r.db.Create(&request)
	return strconv.Itoa(int(request.ID)), errors.DBError(res.Error)

}

func (r repository) EndSession(sessionId string) {
	return
}

func (r repository) InsertActivationCode(ID uint, code string) *errors.Error {
	if ID == 0 {
		return errors.InvalidRequestData()
	}
	entity := Member{}
	entity.ID = ID
	res := r.db.Model(&entity).Update("activationCode", code)
	return errors.DBError(res.Error)
}

func (r repository) StartSession(sessionId string) *errors.Error {
	return nil
}

func (r repository) FindByID(ID uint, email string) (Member, *errors.Error) {
	var ad Member
	if ID != 0 {
		x := r.db.First(&ad, ID)
		return ad, errors.DBError(x.Error)
	}

	if email != "" {
		x := r.db.First(&ad, "email = ?", email)
		return ad, errors.DBError(x.Error)
	}

	return Member{}, nil
}


func (r repository) FindAll() ([]Member, *errors.Error) {
	var members []Member
	res := r.db.Find(&members)

	return members, errors.DBError(res.Error)
}


func (r repository) UpdateInfo(request Member) *errors.Error {
	if request.ID != 0 {
		res := r.db.Model(&request).Updates(request)
		return errors.DBError(res.Error)
	}
	return nil
}
/*
func (r repository) UpdateProfileImage(userId uint, url string) *errors.Error {
	_, err := r.db.Exec(profileImage, userId, url)
	return errors.DBError(err)
}
*/