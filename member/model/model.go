package model

type Member struct {
	ID          string  `json:"id,omitempty"`
	Email       string  `json:"email"`
	FirstName   *string `json:"firstName"`
	LastName    *string `json:"lastName"`
	IsActive    bool    `json:"isActive"`
	IsAdmin     bool    `json:"isAdmin"`
	PhoneNumber *string `json:"phoneNumber"`
	PhotoUrl    *string `json:"photoUrl"`
	Password    string  `json:"password,omitempty"`
	Address     *string `json:"address"`
	TownId      uint    `json:"townId,omitempty"`
	Token       string  `json:"token"`
	CreatedBy   uint    `json:"createdBy,omitempty"`
}
