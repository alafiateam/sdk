package service

import (
	"context"
	"log"
	"strconv"
	"strings"

	"github.com/sethvargo/go-password/password"
	appContext "gitlab.com/alafiateam/sdk/context"
	"gitlab.com/alafiateam/sdk/errors"
	"gitlab.com/alafiateam/sdk/jwt"
	"gitlab.com/alafiateam/sdk/member/model"
	"gitlab.com/alafiateam/sdk/member/repository"
	"gitlab.com/alafiateam/sdk/utils"
	"golang.org/x/crypto/bcrypt"
)

func (self *service) Active(ID, code string) *errors.Error {
	result, err := self.repository.FindByID(utils.ID2Int(ID), "")
	if err != nil {
		log.Println(err)
		return errors.InvalidRequestData()
	}

	if err := comparePassword(result.ActivationCode, code); err != nil {
		log.Println(err)
		return errors.Unauthorized()
	}
	return self.repository.Active(utils.ID2Int(ID))
}

func (self *service) ChangePassword(ID, oldPassword, newPassword string) *errors.Error {
	if oldPassword == "" || newPassword == "" {
		return errors.InvalidRequestData()
	}
	result, err := self.repository.FindByID(utils.ID2Int(ID), "")
	if err != nil {
		log.Println(err)
		return errors.InvalidRequestData()
	}
	if err := comparePassword(result.Password, oldPassword); err != nil {
		log.Println(err)
		return errors.Unauthorized()
	}
	hashNewPass, _ := utils.HashPassword(newPassword)
	return self.repository.ChangePassword(utils.ID2Int(ID), hashNewPass)
}

func (self *service) Create(request model.Member) (string, string, *errors.Error) {

	hashPass, _ := utils.HashPassword(request.Password)
	activationCode := strconv.Itoa(utils.GenerateCode(false))
	hashCode, _ := utils.HashPassword(activationCode)
	query := repository.Member{
		Email:          strings.ToLower(request.Email),
		Password:       hashPass,
		PhoneNumber:    request.PhoneNumber,
		FirstName:      request.FirstName,
		LastName:       request.LastName,
		ActivationCode: hashCode,
		Address:        request.Address,
		IsAdmin:        request.IsAdmin,
	}

	createdId, err := self.repository.Create(query)
	if err != nil {
		return "", "", err
	}

	return createdId, activationCode, nil
}

func (self *service) UpdateInfo(ctx context.Context, request model.Member) (model.Member, *errors.Error) {

	query := repository.Member{
		Email:       request.Email,
		PhoneNumber: request.PhoneNumber,
		FirstName:   request.FirstName,
		LastName:    request.LastName,
		Address:     request.Address,
	}
	query.ID = utils.ID2Int(request.ID)
	if err := self.repository.UpdateInfo(query); err != nil {
		return model.Member{}, errors.InvalidRequestData()
	}
	return self.accountInfo(request.ID)
}

func (self *service) Login(email, password string) (model.Member, *errors.Error) {

	result, err := self.repository.FindByID(0, strings.ToLower(email))
	if err != nil {
		log.Println(err)
		return model.Member{}, errors.Unauthorized()
	}

	if comparePassword(result.Password, password) != nil {
		return model.Member{}, errors.Unauthorized()
	}

	tokenString, er := jwt.GenerateToken(result.ID, result.IsAdmin, result.IsActive, email, "")
	if er != nil {
		log.Println(er)
		return model.Member{}, errors.Unauthorized()
	}
	//if err = self.repository.StartSession(tokenString); err != nil {
	//	return model.LoginResponse{}, errors.InvalidRequestData()
	//}

	return model.Member{
		Token:       tokenString,
		FirstName:   result.FirstName,
		LastName:    result.LastName,
		Email:       email,
		IsActive:    result.IsActive,
		PhotoUrl:    result.PhotoUrl,
		PhoneNumber: result.PhoneNumber,
		Address:     result.Address,
		IsAdmin:     result.IsAdmin,
	}, nil
}

func (self *service) Logout(ctx context.Context) {
	ctxValues := appContext.ContextKeys(ctx)
	self.repository.EndSession(ctxValues.Token)
}

func (self *service) GenerateCode(userID string) ([3]string, *errors.Error) {
	ret := [3]string{}
	result, _ := self.repository.FindByID(utils.ID2Int(userID), "")
	if len(result.Email) == 0 {
		return ret, errors.Unknown()
	}
	activationCode := strconv.Itoa(utils.GenerateCode(false))
	hashCode, _ := utils.HashPassword(activationCode)
	if err := self.repository.InsertActivationCode(utils.ID2Int(userID), hashCode); err != nil {
		return ret, err
	}
	ret[0] = result.Email
	ret[1] = *result.FirstName
	ret[2] = activationCode

	return ret, nil

}

/*
func (self *service) Member(ctx context.Context) (model.Member, *errors.Error) {
	ctxValue := appContext.ContextKeys(ctx)
	return self.accountInfo(ctxValue.MemberId)
}
*/
func (self *service) accountInfo(ID string) (model.Member, *errors.Error) {
	v, err := self.repository.FindByID(utils.ID2Int(ID), ID)
	if err != nil {
		return model.Member{}, err
	}
	if v.Email == "" {
		return model.Member{}, nil
	}
	return model.Member{
		//ID:          strconv.Itoa(int(v.ID)),
		Email:       v.Email,
		FirstName:   v.FirstName,
		LastName:    v.LastName,
		IsActive:    v.IsActive,
		PhoneNumber: v.PhoneNumber,
		PhotoUrl:    v.PhotoUrl,
	}, nil
}

func comparePassword(hashedPassword, password string) error {
	return bcrypt.CompareHashAndPassword([]byte(hashedPassword), []byte(password))
}

func (self *service) ForgotPassword(email string) (string, string, *errors.Error) {
	result, _ := self.repository.FindByID(0, email)
	if result.Email != email {
		return "", "", errors.ForgotPassword()
	}
	passwd, _ := password.Generate(6, 1, 0, false, false)
	hashPass, _ := utils.HashPassword(passwd)
	err := self.repository.ChangePassword(result.ID, hashPass)
	if err != nil {
		log.Println(err)
		return "", "", errors.ForgotPassword()
	}

	return passwd, *result.FirstName, nil
}

func (self *service) EmailChange(userId, email string) (string, string, *errors.Error) {
	result, err := self.repository.FindByID(utils.ID2Int(userId), userId)
	if err != nil || result.Email == "" || result.Email == email {
		log.Println(err)
		return "", "", errors.Unauthorized()
	}
	activationCode := strconv.Itoa(utils.GenerateCode(false))
	hashCode, _ := utils.HashPassword(activationCode)
	if err = self.repository.ChangeEmail(utils.ID2Int(userId), email, hashCode); err != nil {
		log.Println(err)
		return "", "", errors.UnknownData()
	}

	return activationCode, *result.FirstName, nil
}

/*
func (self *service) UpdateProfileImage(ID, url string) (model.Member, *string, *errors.Error) {
	u, _ := self.accountInfo(ID)
	old := u.PhotoUrl
	err := self.repository.UpdateProfileImage(utils.ID2Int(ctxValue.MemberId), url)
	if err != nil {
		return model.Member{}, nil, err
	}
	u, err = self.accountInfo(ctxValue.MemberId)
	return u, old, err
}
*/

func (self *service) Members() []model.Member {
	res, err := self.repository.FindAll()
	if err != nil {
		log.Println(err)
		return nil
	}
	members := make([]model.Member, len(res))
	for k, v := range res {
		members[k] = model.Member{
			ID:          strconv.Itoa(int(v.ID)),
			Email:       v.Email,
			FirstName:   v.FirstName,
			LastName:    v.LastName,
			IsActive:    v.IsActive,
			IsAdmin:     v.IsAdmin,
			PhoneNumber: v.PhoneNumber,
			PhotoUrl:    v.PhotoUrl,
			Address:     v.Address,
		}
	}
	return members
}
