package service

import (
	"context"

	"gitlab.com/alafiateam/sdk/errors"
	"gitlab.com/alafiateam/sdk/member/model"
	"gitlab.com/alafiateam/sdk/member/repository"
)

//the context may have all needed keys to perform operation
//see context package for keys

//function are sorted alphabetically
type Service interface {
	//active current user
	//activation code must be sent by the context
	Active(ID, code string) *errors.Error

	//change the current user password
	ChangePassword(ID, oldPassword, newPassword string) *errors.Error

	//user creation
	//user id and activation code are returned
	Create(request model.Member) (string, string, *errors.Error)

	//change uer info
	UpdateInfo(ctx context.Context, request model.Member) (model.Member, *errors.Error)

	//user login
	Login(email, password string) (model.Member, *errors.Error)

	//user logout
	Logout(ctx context.Context)

	//generate a new activation code to the user
	GenerateCode(userID string) ([3]string, *errors.Error)

	//get user info
	//Account(ctx context.Context) (model.Account, *errors.Error)

	//retrieve users list of a resource
	Members() []model.Member

	ForgotPassword(email string) (string, string, *errors.Error)

	//UpdateProfileImage(ID, url string) (model.Member, *string, *errors.Error)

	EmailChange(userId, email string) (string, string, *errors.Error)
}

type service struct {
	repository repository.Repository
}

func NewService(repo repository.Repository) Service {
	return &service{
		repository: repo,
	}
}
